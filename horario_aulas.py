def horario(semana, mat, x=0):
    if len(semana) > x:
        insMateria(semana[x], mat)
        return horario(semana, mat, x+1)
    else:
        return semana

def verificaProfessor(dia, professor):
    if dia in professor:
        return True
    else:
        return False

def insMateria(dia, materias, x=0):
    if len(dia) < 3:
        if materias[x][1] == 0:
            return insMateria(dia, materias, x+1)
        else:
            if verificaProfessor(dia[0], varreProf(materias[x][2], professores)):
                dia.append(materias[x][0])
                materias[x][1] -= 1
                return insMateria(dia, materias, x)
            else: 
                return insMateria(dia, materias, x+1)
    else:
        return dia

def varreProf(nome, lista, x=0):
    if nome == lista[x][0]:
        return lista[x]
    else:
        return varreProf(nome, lista, x+1)


semana = [["Segunda-Feira"],["Terça-Feira"],["Quarta-Feira"],["Quinta-Feira"],["Sexta-Feira"]]

professores = [("Eloiza","Segunda-Feira","Terça-Feira"),("Faulin","Segunda-Feira","Terça-Feira"),("César","Quarta-Feira","Quinta-Feira"),("Victor Borba","Quarta-Feira","Quinta-Feira"),("Cléber","Segunda-Feira","Sexta-Feira"),("Éttore","Terça-Feira","Quarta-Feira","Sexta-Feira")]

quarto = [["Inglês IV",1,"Eloiza"],["Manejos Agrícolas de Trato Culturais e Colheita",1,"Faulin"],["Redes de Computadores",2,"Éttore"],["Empreendedorismo",1,"César"],["Algoritmos Avançados",3,"Victor Borba"],["Web Semântica",2,"Cléber"]]

terceiro = []

segundo = []

primeiro = []

horarioQuarto = horario(semana, quarto)

print(horarioQuarto)

# encoding: utf-8


def criaMatriz(size, valorPadrao=0):
    h, w = size if isinstance(size,(tuple,)) else (size,size)
    line = [valorPadrao] * w
    matriz = list(list(line) for i in range(h))
    return matriz

class Matriz:
    def __init__(self,n=10,valorPadrao=0):
        self.__n = n
        self.__matriz = criaMatriz(n,valorPadrao)

    def imprimeMatriz(self):
        for i in range(len(self.__matriz)):
            print (self.__matriz[i])
        return self.__matriz

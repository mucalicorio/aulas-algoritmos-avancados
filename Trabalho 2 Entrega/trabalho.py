from leitura_csv import LeArquivo
from criacao_csv import CriaArquivo


def vertices_adj(grafo, vertice):
    adj = []
    for i in range(1, len(grafo[vertice])):
        adj.append(grafo[vertice][i])
    return adj


def agendar_semana(grafo, semana, fila, visitados, professores_materias, vertice):
    if vertice < 10 and vertice not in visitados:
        fila.append(grafo[vertice])
        while len(fila) > 0:
            v = fila.pop(0)
            adj = vertices_adj(grafo, v[0])
            for vizinho in range(len(adj)):
                if v[0] not in visitados:
                    aula = verifica_aula(encontra_valor(
                        legenda_1, adj[vizinho]), professores_materias)
                    if aula != None:
                        visitados.append(grafo[vertice][0])
                        dia = []
                        for key, value in legenda_1.items():
                            if v[0] == key:
                                dia.append(value)
                        dia.append(aula)
                        semana.append(dia)
                    elif vizinho+1 == len(adj):
                        visitados.append(grafo[vertice][0])

        return agendar_semana(grafo, semana, fila, visitados, professores_materias, vertice+1)
    elif len(visitados) < 10:
        return agendar_semana(grafo, semana, fila, visitados, professores_materias, 0)
    else:
        return semana


def encontra_valor(dicionario, valor):
    return dicionario.get(valor)


def verifica_aula(professor, lista):
    for obj in lista:
        if professor in obj and obj[1][1] != 0:
            obj[1][1] -= 2
            return obj[1][0]


def ler_professores(resultado_csv, legenda):
    dia = 0
    try:
        while dia < len(resultado_csv):
            legenda[(10 + dia)] = resultado_csv[dia][0]
            dia += 1
    except:
        print('Não conseguimos ler o arquivo.')
        quit()


def adiciona_vertice_professor(semana_professores, legenda, resultado_csv):
    try:
        for professor in resultado_csv:
            for chave, valor in legenda.items():
                if professor[0] == valor:
                    semana_professores.append([chave])
                    adjacencia_professor(semana_professores,
                                         legenda, professor, chave)
                    break
                else:
                    pass
    except:
        print('Não conseguimos ler o arquivo.')
        quit()


def adjacencia_professor(semana_professores, legenda, professor, chave):
    aulas = professor[1].split(',')
    for aula in aulas:
        for key, value in legenda.items():
            if aula == value:
                semana_professores[chave].append(key)


print('Olá. Seja bem-vindo!\nPara fazer a organização da agenda:\n1º) Use o padrão passado pelo Professor Victor Borba.\n2º) Salve o arquivo com o nome "disponibilidadeX.csv", onde X é o termo (por exemplo, "5", para 5º termo).\n\nO resultado retornará no próprio terminal, e também gerará um arquivo csv, com nome "aulaX.csv", na mesma regra do ponto anterior.\n')
termo = input('Termo: ')

try:
    resultado_csv = LeArquivo().retorna_conteudo(termo)
except:
    print('Não conseguimos ler o arquivo.')
    quit()

legenda_1 = {
    0: 'segundaA',
    1: 'segundaD',
    2: 'tercaA',
    3: 'tercaD',
    4: 'quartaA',
    5: 'quartaD',
    6: 'quintaA',
    7: 'quintaD',
    8: 'sextaA',
    9: 'sextaD'
}

semana_professores = [
    [0],
    [1],
    [2],
    [3],
    [4],
    [5],
    [6],
    [7],
    [8],
    [9]
]

professor_legenda = 10

ler_professores(resultado_csv, legenda_1)

adiciona_vertice_professor(semana_professores, legenda_1, resultado_csv)

for dia in range(10):
    try:
        for disponibilidade_professor in resultado_csv:
            if legenda_1[dia] in disponibilidade_professor[1]:
                for chave, valor in legenda_1.items():
                    if disponibilidade_professor[0] == valor:
                        semana_professores[dia].append(chave)
                        professor_legenda += 1
    except:
        print('Não conseguimos ler o arquivo.')
        quit()


def tentativa_dia_inicial(x):
    semana = []
    fila = []
    visitados = []

    try:
        professores_materias = [
            [resultado_csv[0][0], [resultado_csv[0][2], resultado_csv[0][3]]],
            [resultado_csv[1][0], [resultado_csv[1][2], resultado_csv[1][3]]],
            [resultado_csv[2][0], [resultado_csv[2][2], resultado_csv[2][3]]],
            [resultado_csv[3][0], [resultado_csv[3][2], resultado_csv[3][3]]],
            [resultado_csv[4][0], [resultado_csv[4][2], resultado_csv[4][3]]],
            [resultado_csv[5][0], [resultado_csv[5][2], resultado_csv[5][3]]]
        ]
    except:
        print('Não conseguimos ler o arquivo.')
        quit()

    agenda = agendar_semana(semana_professores, semana,
                            fila, visitados, professores_materias, x)
    if len(agenda) == 10:
        return agenda
    else:
        faltantes = []
        for professor in professores_materias:
            if professor[1][1] != 0:
                faltantes.append(professor)
        return str(faltantes)


faltantes = []
try:
    for y in range(10):
        ass = type(tentativa_dia_inicial(y))
        if type(tentativa_dia_inicial(y)) == str:
            faltantes.append(tentativa_dia_inicial(y))
        elif type(tentativa_dia_inicial(y)) == list:
            CriaArquivo().organiza_dados(termo, tentativa_dia_inicial(y))
        else:
            pass
    
    print('\nFALTANTES NAS TENTATIVAS\n')
    print(faltantes)
except:
    print('Não conseguimos agendar.')

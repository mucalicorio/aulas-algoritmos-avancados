import csv


class CriaArquivo:
    def cria_arquivo(self, termo):
        arquivo = open('aulas%s.csv' % termo, 'w')
        return arquivo

    def salva_arquivo(self, arquivo, dados):
        arquivo.write(dados)
        # print(dados)

    def organiza_dados(self, termo, dados):
        dia_semana = []
        arquivo = CriaArquivo().cria_arquivo(termo)

        for linha in dados:
            dia_semana = linha[0]
            if dia_semana == 'segundaA':
                dia_semana = 'segunda,13,15'
            if dia_semana == 'segundaD':
                dia_semana = 'segunda,15,17'
            if dia_semana == 'tercaA':
                dia_semana = 'terca,13,15'
            if dia_semana == 'tercaD':
                dia_semana = 'terca,15,17'
            if dia_semana == 'quartaA':
                dia_semana = 'quarta,13,15'
            if dia_semana == 'quartaD':
                dia_semana = 'quarta,15,17'
            if dia_semana == 'quintaA':
                dia_semana = 'quinta,13,15'
            if dia_semana == 'quintaD':
                dia_semana = 'quinta,15,17'
            if dia_semana == 'sextaA':
                dia_semana = 'sexta,13,15'
            if dia_semana == 'sextaD':
                dia_semana = 'sexta,15,17'

            auxiliar = dia_semana + ',' + linha[1] + '\n'
            dia_semana = dia_semana.split(',')
            print ('Dia Semana: ', dia_semana[0])
            print ('Hora Inicial: ', dia_semana[1])
            print ('Hora Final: ', dia_semana[2])
            print ('Matéria: ', linha[1], '\n')

            CriaArquivo().salva_arquivo(arquivo, auxiliar)
        
        arquivo.close()

    def __init__(self):
        pass
        # print('Classe de Criação de Arquivos .csv')

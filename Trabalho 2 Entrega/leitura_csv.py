import csv


class LeArquivo:
    def abre_arquivo(self, termo):
        arquivo = open('disponibilidade%s.csv' % termo)
        return arquivo

    def ler_arquivo(self, arquivo):
        conteudo_arquivo = csv.reader(arquivo, delimiter=',')
        return conteudo_arquivo

    def filtra_conteudo(self, conteudo):
        conteudo_filtrado = []
        for linha in conteudo:
            if linha[0] != 'Professor':
                professor = linha[0]
            else:
                professor = None
            if linha[1] != 'Materia':
                materia = linha[1]
            else:
                materia = None
            if linha[2] != 'Aulas':
                aulas = int(linha[2])
            else:
                aulas = None
            if linha[3] != 'Dia':
                dia = linha[3]
            else:
                dia = None
            if linha[4] != 'Inicio':
                inicio = int(linha[4])
            else:
                inicio = None
            if linha[5] != 'Fim':
                fim = int(linha[5])
            else:
                fim = None

            if professor != None:
                aux = [professor, materia, aulas, dia, inicio, fim]
                conteudo_filtrado.append(aux)

        return list(conteudo_filtrado)

    def substitui_conteudo(self, conteudo_filtrado):
        conteudo_novo = []
        for linha in conteudo_filtrado:
            professor = linha[0]
            materia = linha[1]
            aulas = linha[2]
            dia = linha[3]
            inicio = linha[4]
            fim = linha[5]

            if (inicio == 13) and (fim == 15):
                dia = dia + 'A'
            elif (inicio == 13) and (fim == 17):
                dia = dia + 'A,' + dia + 'D'
            elif (inicio == 15) and (fim == 17):
                dia = dia + 'D'
            else:
                print ('Erro ao Validar Disponibilidade')

            if conteudo_novo != []:
                if professor != conteudo_novo[-1][0] or materia != conteudo_novo[-1][2]:
                    disponibilidade = dia
                    conteudo_novo.append([professor, disponibilidade, materia, aulas])
                else:
                    conteudo_novo[-1][1] = conteudo_novo[-1][1] + ',' + dia
            else:
                conteudo_novo.append([professor, dia, materia, aulas])

        return conteudo_novo

    def retorna_conteudo(self, termo):
        arquivo = self.abre_arquivo(termo)
        conteudo_arquivo = self.ler_arquivo(arquivo)
        conteudo_filtrado = self.filtra_conteudo(conteudo_arquivo)
        conteudo_novo = self.substitui_conteudo(conteudo_filtrado)

        arquivo.close()

        return conteudo_novo

    def __init__(self):
        pass

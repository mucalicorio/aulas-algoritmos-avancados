# Suponha que tenhamos disponíveis moedas com valores de 100, 25, 10, 5 e 1. Cria um algoritmo que para
# conseguir obter um determinado valor com o menor número de moedas possível (problema do troco).
#
# Ex: troco de R$2.89, são necessárias 2 moedas de 100, 3 de 25, 1 de 10 e 4 de 1.

moedas = [
    100,
    25,
    10,
    5,
    1
]

print ('Moedas 1: ', moedas)

total = 0

print ('Total 1: ', total)

troco = 289

print ('Troco 1: ', troco)

contador = 2

for i in range(len(moedas)):
    num_moedas = troco // moedas[i]

    print ('Num_Moedas ', (contador-1))
    print (num_moedas)

    print ('Moedas I: ', (contador))
    print (moedas[i])

    troco -= num_moedas * moedas[i]

    print ('Troco ', (contador))
    print (troco)

    total += num_moedas

    print ('Total ', (contador))
    print (total)

    contador += 1


print('TOTAL DE MOEDAS UTILIZADAS: ', total)

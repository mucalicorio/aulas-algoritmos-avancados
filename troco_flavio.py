moedas = [100,25,10,5,1]

troco = []

def calcTroco(valor, moedas, x=0):
    if valor != 0:
        if valor < moedas[x]:
            return calcTroco(valor,moedas,x+1)
        else:
            troco.append(moedas[x])
            valor -= moedas[x]
            return calcTroco(valor,moedas,x)
    else: return troco

valor = 3.89
print(calcTroco(valor*100, moedas))

# encoding: utf-8

# Percorre todos os vértices definindo a distância = null;
# Define distância = 0 para o vértice inicial;
# Adiciona o vértice inicial em uma fila;
# Enquanto a fila não estiver vazia, faz:
    # Retira o primeiro da fila;
    # Percorre os adjacentes dele;
    # Para cada adjacente não visitado, faz:
        # Se, distância do adjacente = null, faz:
            # Distância do adjacente = distância do vértice + 1.


def percorre_vertices(vertices, distancias):
    for vertice in vertices:
        distancias[vertice][1] = None

def zera_inicial(vertice_inicial, distancias):
    distancias[vertice_inicial][1] = 0

def inicia_fila(vertice_inicial, fila):
    fila.append(vertice_inicial)

def vertices_vizinhos(grafo, vertice):
    adj = []
    try:
        for i in range(1, len(grafo[vertice])):
            print ("oi")
            adj.append(grafo[vertice][i])
    except:
        print ('o')
    return adj


vertices = [0, 1, 2, 3, 4, 5, 6]

distancias = [
    [0, None],
    [1, None],
    [2, None],
    [3, None],
    [4, None],
    [5, None],
    [6, None]
]

lista_adjacencias = [
    [0, 1, 3],
    [1, 3, 4],
    [2, 0, 5],
    [3, 2, 4, 5, 6],
    [4, 6],
    [6, 5, 7],
    [7, 3, 4]
]

# lista_adjacencias = [
#     [[0,-1],[1,-1],[3,-1]],
#     [[1,-1],[3,-1],[4,-1]],
#     [[2,-1],[0,-1],[5,-1]],
#     [[3,-1],[2,-1],[4,-1],[5,-1],[6,-1]],
#     [[4,-1],[6,-1]],
#     [[5,-1]],
#     [[6,-1],[5,-1]]
# ]

vertice_inicial = 2

fila = []

percorre_vertices(vertices, distancias)

zera_inicial(vertice_inicial, distancias)

inicia_fila(vertice_inicial, fila)

while len(fila) > 0:
    vertice_retirado = fila.pop(0)

    print (vertice_retirado)

    visitados = [vertice_inicial]
    
    for vizinho in vertices_vizinhos(lista_adjacencias, vertice_retirado):
        if not vizinho in visitados:
            vizinho = vertice_retirado + 1
            fila.append(vizinho)
            distancias.append(vizinho)
            visitados.append(vizinho)

print ('Distâncias em Largura: ', distancias)

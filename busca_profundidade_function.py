# encoding: utf-8

def buscaProfundidade(grafo, atual):
	visitados = []
	vizinhos = [atual]
	j = 0

	visitados.append(int(atual))

	while vizinhos != []:
		try:
			vizinhos = verticesAdj(grafo, int(visitados[j]))

			i = 0
			for vizinho in vizinhos:
				if vizinho not in visitados:
					atual = vizinho
					break

				else:
					i += 1

			if i == len(vizinhos):
				k = 0
				for vertice in grafo:
					if vertice[0] in visitados:
						k += 1
						
					else:
						m = 1
						while m < len(visitados):
							vizinhos_fernando = verticesAdj(grafo, int(visitados[j-m]))

							l = 0
							find = False
							for vizinho_fernando in vizinhos_fernando:
								if vizinho_fernando not in visitados:
									atual = visitados[j-m]
									find = True
									break

								else:
									l += 1

							if l == len(vizinhos_fernando):
								m += 1

							if find:
								break

				if len(grafo) == k:
					vizinhos = []
					resultado = visitados

			visitados.append(int(atual))
			j += 1

		except:
			print ('--'*45)
			print ('TRAVOU')
			break

	return resultado

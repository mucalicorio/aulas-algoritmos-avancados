# encoding: utf-8

import csv
from base_matriz import Matriz

Novo = open('listaAdj.csv','w')
class Aresta:
    def inserirAresta(self, matriz,x,y):
        matriz[x][y] = 1
        matriz[y][x] = 1

        return matriz


class Adjacencia:
    def vertAdj(self,x):
        adj = []
        for i in range(len(matriz[x])):
            if matriz[x][i]==1:
                adj.append(i)
        return adj

    def listaAdj(self):
        lista = []
        for i in range(len(matriz)):
            concatena = []
            concatena.append(i)
            concatena.append(self.vertAdj(i))
            lista.append(concatena)
        return lista


def padronizaParaCsv(padrao):
    # 0;1:1;0,4:2;3,4:3;2,5:4;1,2,7:5;3,6:6;5,10:7;4,9:8;9,11:9;7,8:10;6,11:11;8,10
    # [[0, []], [1, [2, 3, 4]], [2, [1, 3]], [3, [1, 2]], [4, [1]]]
    # 0,1,4;1,0;2,3;3,2;4,0;5;6;7;8;9;10;11

    padrao = str(padrao)
    padrao = padrao.replace('],', ';')
    padrao = padrao.replace(', []', '')
    padrao = padrao.replace(' ', '')
    padrao = padrao.replace('[', '')
    padrao = padrao.replace(']', '')

    return padrao


valor = input('Olá! Digite a ordem da matriz: ')
print ('-'*53)
valor = int(valor)

matrizClass = Matriz(valor)
arestaClass = Aresta()
 
matriz = matrizClass.imprimeMatriz()

sairWhile = ''

while sairWhile != 's':
    print ('-'*53)
    x, y = input('Aresta (x,y): ').split(',')
    print ('-'*53)
    x = int(x)
    y = int(y)

    matriz = arestaClass.inserirAresta(matriz,x,y)
    matrizClass.imprimeMatriz()

    print ('-'*53)
    sairWhile = input('Digite "s" para parar de adicionar arestas: ')

print ('*'*53)
print ('*'*53)
print ('')
print ('')

adjClass = Adjacencia()

sairWhile = ''

while sairWhile != 's':
    valor = input('Digite o vértice a ser analisada a existência de adjacência: ')
    print ('-'*53)
    valor = int(valor)
    vert = adjClass.vertAdj(valor)

    vertLimpa = str(vert)
    vertLimpa = vertLimpa.replace('[', '')
    vertLimpa = vertLimpa.replace(']', '')
    print ('Vértices adjacentes à ' + str(valor) + ': ' + vertLimpa)
    del vertLimpa
    
    print ('Grau do vértice: ' + str(len(vert)))
    print ('-'*53)

    v1, v2 = input('Vértices (v1,v2): ').split(',')
    v1 = int(v1)
    v2 = int(v2)
    print ('-'*53)

    verify = False
    for i in adjClass.vertAdj(v1):
        if i == v2:
            verify = True

    print (str(v1) + ' é adjacente à ' + str(v2) + '? ' + str(verify))

    print ('-'*53)
    sairWhile = input('Digite "s" para parar de verificar as adjacências: ')
    if sairWhile != 's':
        print ('-'*53)

padronizado = padronizaParaCsv(adjClass.listaAdj())

print ('Lista de Adjacências: ')
print (padronizado)
print ('-'*53)

Novo.write(str(padronizado))
Novo.close()

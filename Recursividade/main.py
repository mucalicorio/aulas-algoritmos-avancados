# encoding: utf-8

import functions
import os


escolha = 0

largura, altura = os.get_terminal_size()

while escolha != '7':
    functions.clearTerminal()
    escolha = functions.escolhas()

    if escolha == '1':
        functions.clearTerminal()
        x, y = input('-'*largura + '\nX vezes Y (x,y): ').split(',')
        x = float(x)
        y = float(y)
        print (functions.multiplica(x, y))
        ok = input('-'*largura)

    elif escolha == '2':
        functions.clearTerminal()
        x, y = input('-'*largura + '\nX elevado a Y (x,y): ').split(',')
        x = int(x)
        y = int(y)
        fatora = int(functions.fatora(x, y))
        print (str(x) + ' ^ ' + str(y) + ' = ' + str(fatora))
        ok = input('-'*largura)

    elif escolha == '3':
        functions.clearTerminal()
        x = input('-'*largura + '\nValor para somar os anteriores (x): ')
        x = float(x)
        print (functions.somaAnteriores(x))
        ok = input('-'*largura)

    elif escolha == '4':
        functions.clearTerminal()
        x = input('-'*largura + '\nDigite o número para ver o primeiro algarismo dele (x): ')
        x = float(x)
        print (functions.primeiroAlgarismo(x))
        ok = input('-'*largura)

    elif escolha == '5':
        functions.clearTerminal()
        lista = [10,20,40,35,320,123, 10, 20, 43, 321, 423, 425, 5421, 234, 10947, 18273, 1, 2, 3, 4, 3, 2, 1, 3, 2, 1, 4, 3, 2, 3, 4, 123, 321, 432, 35]
        print (lista)
        x = input('-'*largura + '\nDigite o número a ser removido da lista (x): ')
        x = int(x)
        print (functions.removeItemLista(lista, x))
        ok = input('-'*largura)

    elif escolha == '6':
        functions.clearTerminal()
        x = input('-'*largura + '\nDigite o número a verificada a perfeição (x): ')
        x = float(x)
        print (functions.numeroPerfeito(x))
        ok = input('-'*largura)
        functions.clearTerminal()

    elif escolha == '7':
        print (('-'*largura + '\n'+ ' '*50 + 'Poxa, não vai embora não :(\n' + '-'*largura)*(int(float(altura)/3)))
        ok = input()
        functions.clearTerminal()

    else:
        functions.clearTerminal()
        print ('Opção inválida.')
        functions.escolhas()

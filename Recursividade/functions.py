# encoding: utf-8

import os

larguraTerminal, alturaTerminal = os.get_terminal_size()

def clearTerminal():
    return os.system('cls' if os.name == 'nt' else 'clear')
    

def escolhas():
    print ('Olá, queridão! Seja bem-vindo!\n\n' + '-'*larguraTerminal + '\n\nPor favor, leia os exercícios feitos nessa aplicação:\n\n')
    print ('1 - Elabore uma função recursiva para calcular o valor de X multiplicando por Y (sem usar multiplicação *).\n' + '-'*larguraTerminal)
    print ('2 - Elabore uma função recursiva para calcular o valor de X elavado a Y (sem usar ** ou a função pow).\n' + '-'*larguraTerminal)
    print ('3 - Elabore uma função recursiva para calcular o valor da soma (somatório de números): S = 1+2+3+4+5+6+...+ n (para n > 0).\n' + '-'*larguraTerminal)
    print ('4 - Implemente uma função recursiva que retorne o primeiro algarismo (o mais significativo) na representação decimal de N).\n' + '-'*larguraTerminal)
    print ('5 - Implemente uma função recursiva que recebe como parâmetro uma lista de números inteiros L e um número inteiro N e retorna a lista que resulta de apagar de L todas as ocorrências de N.\n' + '-'*larguraTerminal)
    print ('6 - Implemente uma função recursiva que recebe como parâmetro um número inteiro positivo e devolve True se esse número for um número perfeito e False em caso contrário.\n *Recorde que um número perfeito é um úmero natural que é igual à soma de todos os seus divisores próprios, isto é, a soma de todos os divisores excluindo o próprio número.\n' + '-'*larguraTerminal)
    print ('7 - Sair.\n' + '-'*larguraTerminal)
    
    exercicio = input('\nDigite o número do exercício a ser testado: ')

    return exercicio


def multiplica(x, y):
    if (y == 1):
        return x
    else:
        return x + multiplica(x, y-1)


def fatora(x, y):
    if (y == 1):
        return x
    else:
        return x * fatora(x, y-1)


def somaAnteriores(x):
    if (x == 1):
        return x
    else:
        return x + somaAnteriores(x-1)

def primeiroAlgarismo(x):
    x = int(x)
    x = str(x)
    if (len(x) == 1):
        return x
    else:
        x = float(x)
        return primeiroAlgarismo(x/10)

def removeItemLista(lista, item):
    if item not in lista:
        return lista
    else:
        lista.pop(lista.index(item))
        return removeItemLista(lista, item)

def numeroPerfeito(x, soma=0, divide=1):
    if x > divide:
        if x % divide == 0:
            soma += divide
            
        return numeroPerfeito(x, soma, divide+1)

    elif x == divide and x == soma:
        return True
        
    else:
        return False

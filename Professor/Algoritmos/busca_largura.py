#CODIGO APENAS DAS FUNÇÕES SEM O MAIN DA APLICAÇÃO, A FUNÇÃO buscaLargura DEVE RECEBER UMA LISTA DE ADJACENCIAS DE UM GRAFO E O VERTICE DE INICIO DA BUSCA
def verticesAdj(grafo, vertice):
	adj = []
	for i in range(1, len(grafo[vertice])):
		adj.append(grafo[vertice][i])
	return adj

def buscaLargura(grafo, vertice):
    fila = []
    # insere primeiro vertice na fila e marca como visitado
    fila.append(vertice)
    visitados = [vertice]

    # percorrer todos os vertices (Obs.: Somente grafos conexos)
    while len(fila):
        v = fila.pop(0)  # pega o proximo vertice da fila
        # percorrer vizinhos
        for vizinho in verticesAdj(grafo,v):            
            # verifica se ja foi visitado, caso nao, faz a visita e coloca na fila para verificar os vizinhos
            if not vizinho in visitados:
                # ... colocamos na fila para visitar
                fila.append(vizinho)
                visitados.append(vizinho)

            # MOMENTO da visita
            print v, '->', vizinho
    print 'fila: ',fila
    print 'visitados: ',visitados

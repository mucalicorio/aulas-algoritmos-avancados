#!/usr/bin/python
# -*- encoding: utf-8 -*-

def geraMatriz():
	n = raw_input("Quantidade de Vertices:")
	valor_padrao = raw_input("Valor padrão:")
	matriz=[]
	for i in range(int(n)):
		matriz.append([])
		for j in range(int(n)):
			matriz[i].append(int(valor_padrao))
	imprimeMatriz(matriz)
	return matriz

def imprimeMatriz(mat):
    for m in mat:
        print m

def insereAresta(mat):
	x,y = raw_input("Informe a aresta:").split()
	x = int(x)
	y = int(y)
	peso = raw_input("Peso:")
	mat[x][y] = int(peso)
	mat[y][x] = int(peso)
	imprimeMatriz(mat)

def verticesAdj(mat,vertice):	
	adj=[]
	for i in range(len(mat)):
		if mat[vertice][i]==1:
			adj.append(i)
	return adj

def existeAresta(mat,x,y):
    if mat[x][y]==1:
		return True
    else:
        return False

def grauVertice(mat,vertice):
	if existeAresta(mat,vertice,vertice): #verifica se existe laço
		return len(verticesAdj(mat,vertice))+1
	else:
		return len(verticesAdj(mat,vertice))

menu = {}
menu['1']="Criar Matriz" 
menu['2']="Insere Aresta"
menu['3']="Visualiza Matriz"
menu['4']="Vertices Adjacentes"
menu['5']="Existe Aresta Entre"
menu['6']="Grau do Vertice"
menu['7']="Sair"
grafo = []
while True: 
	options=menu.keys()
	options.sort()
	print '\n'
	for entry in options:
		print entry, menu[entry]
	selection=raw_input("Selecione a opção:")
	if selection =='1':
		grafo = geraMatriz()
	elif selection == '2':
		insereAresta(grafo)
	elif selection == '3':
		imprimeMatriz(grafo)
	elif selection == '4':
		vertice = raw_input("Informe o vertice:")
		print 'Vertices adjacentes a ',vertice,': ',verticesAdj(grafo,int(vertice))
	elif selection == '5':
		(v1,v2) = raw_input("Informe os vertices:")
		if existeAresta(grafo,int(v1),int(v2)):
			print 'Existe!'
		else:
			print 'Não existe!'
	elif selection == '6':
		vertice = raw_input("Informe o vertice:")
		print 'Grau do vertice ',vertice,':',grauVertice(grafo,int(vertice))
	elif selection == '7':
		break
	else:
		print "Opção selecionada INVÁLIDA!" 

